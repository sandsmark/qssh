/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: http://www.qt-project.org/
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**************************************************************************/

#ifndef SFTPDEFS_H
#define SFTPDEFS_H

#include "ssh_global.h"

#include <QFile>
#include <QString>

/*!
 * \namespace QSsh
 * \brief The namespace used for the entire library
 */
namespace QSsh {


/*!
 *\brief Unique ID used for tracking individual jobs.
 */
typedef quint32 SftpJobId;

/*!
    Special ID representing an invalid job, e. g. if a requested job could not be started.
*/
QSSH_EXPORT extern const SftpJobId SftpInvalidJob;


/*!
 * \brief The behavior when uploading a file and the remote path already exists
 */
enum SftpOverwriteMode {
    /*! Overwrite any existing files */
    SftpOverwriteExisting,

    /*! Append new content if the file already exists */
    SftpAppendToExisting,

    /*! If the file or directory already exists skip it */
    SftpSkipExisting
};

/*!
 * \brief The type of a remote file.
 */
enum SftpFileType { FileTypeRegular, FileTypeDirectory, FileTypeOther, FileTypeUnknown };

/*!
 * \brief Possible errors.
*/
enum SftpError { NoError, EndOfFile, FileNotFound, PermissionDenied, GenericFailure, BadMessage, NoConnection, ConnectionLost, UnsupportedOperation  };

/*!
    \brief Contains information about a remote file.
*/
class QSSH_EXPORT SftpFileInfo
{
public:
    SftpFileInfo() : type(FileTypeUnknown), sizeValid(false), permissionsValid(false) { }

    /// The remote file name, only file attribute required by the RFC to be present so this is always set
    QString name;

    /// The type of file
    SftpFileType type = FileTypeUnknown;

    /// The remote file size in bytes.
    quint64 size = 0;

    /// The permissions set on the file, might be empty as the RFC allows an SFTP server not to support any file attributes beyond the name.
    QFileDevice::Permissions permissions{};

    /// Last time file was accessed.
    quint32 atime = 0;

    /// Last time file was modified.
    quint32 mtime = 0;

    /// If the timestamps (\ref atime and \ref mtime) are valid, the RFC allows an SFTP server not to support any file attributes beyond the name.
    bool timestampsValid = false;

    /// The RFC allows an SFTP server not to support any file attributes beyond the name.
    bool sizeValid = false;

    /// The RFC allows an SFTP server not to support any file attributes beyond the name.
    bool permissionsValid = false;
};

/*!
    \brief Contains information about a remote filesystem.
*/
class QSSH_EXPORT SftpVfsInfo
{
public:
    enum Flag : quint64 {
        NoFlags = 0,
        ReadOnly = 1, /** Mount read-only.  */
        NoSuid = 2, /** Ignore suid and sgid bits.  */
        NoDevices = 4, /** Disallow access to device special files. */
        NoExec = 8, /** Disallow program execution. */
        Synchronous = 16, /** Writes are synced at once. */
        AllowMandatoryLock = 64, /** Allow mandatory locks on an FS. */
        Write = 128, /** Write on file/directory/symlink. */
        AppendOnly = 256, /** Append-only file. */
        Immutable = 512, /** Immutable file. */
        NoAccessTime = 1024, /** Do not update access times. */
        NoDirectoryAccessTime = 2048, /** Do not update directory access times. */
        RelativeAccessTime = 4096, /** Update atime relative to mtime/ctime. */
    };

    quint64 blockSize = 0;   /** file system block size */
    quint64 fundamentalBlockSize = 0;  /** fundamental fs block size */

    quint64 blocksTotal = 0;  /** number of blocks (unit f_frsize) */
    quint64 blocksFree = 0;   /** free blocks in file system */
    quint64 blocksAvailable = 0;  /** free blocks for non-root */

    quint64 inodesTotal = 0;   /** total file inodes */
    quint64 inodesFree = 0;   /** free file inodes */
    quint64 inodesAvailable = 0;  /** free file inodes for to non-root */

    quint64 filesystemId = 0;    /** file system id */
    quint64 flags = NoFlags;    /** bit mask of f_flag values */
    quint64 maxFilenameLength = 0; /** maximum filename length */
};

} // namespace QSsh

#endif // SFTPDEFS_H

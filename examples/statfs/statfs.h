/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt Creator.
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#pragma once

#include <QObject>
#include <QSharedPointer>

#include <qssh/sftpchannel.h>
#include <qssh/sshconnection.h>
#include "parameters.h"

namespace QSsh {
class SshConnection;
class SshConnectionParameters;
class SshRemoteProcess;
}

QT_BEGIN_NAMESPACE
class QByteArray;
class QFile;
class QString;
QT_END_NAMESPACE

class StatFs : public QObject
{
    Q_OBJECT
public:
    StatFs(const Parameters &params) : m_parameters(params) {}

    void run();

private slots:
    void handleConnected();
    void handleError();
    void handleDisconnected();
    void handleChannelInitialized();
    void handleChannelInitializationFailure(const QString &reason);
    void handleStatVfsInfoAvailable(QSsh::SftpJobId job, const QSsh::SftpVfsInfo &vfsInfo);
    void handleSftpJobFinished(QSsh::SftpJobId job, const QSsh::SftpError errorType, const QString &error);
    void handleChannelClosed();

private:
    void earlyDisconnectFromHost();

    QSsh::SshConnection *m_connection = nullptr;
    QSsh::SftpChannel::Ptr m_channel;
    QSsh::SftpJobId m_statJob = QSsh::SftpInvalidJob;
    const Parameters m_parameters;
    bool m_error = false;
};

/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt Creator.
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#include "statfs.h"

#include <qssh/sshconnection.h>
#include <qssh/sshremoteprocess.h>

#include <QCoreApplication>
#include <QFile>
#include <QSocketNotifier>

#include <cstdlib>
#include <iostream>

using namespace QSsh;

void StatFs::run()
{
    m_connection = new SshConnection(m_parameters.sshParams, this);
    connect(m_connection, &SshConnection::connected, this, &StatFs::handleConnected);
    connect(m_connection, &SshConnection::error, this, &StatFs::handleError);
    connect(m_connection, &SshConnection::disconnected, this, &StatFs::handleDisconnected);
    std::cout << "Connecting to host '"
        << qPrintable(m_parameters.sshParams.host()) << "'..." << std::endl;
    m_connection->connectToHost();
}

void StatFs::handleConnected()
{
    std::cout << "Connected. Initializing SFTP channel..." << std::endl;
    m_channel = m_connection->createSftpChannel();
    connect(m_channel.data(), &SftpChannel::initialized, this,
       &StatFs::handleChannelInitialized);
    connect(m_channel.data(), &SftpChannel::channelError, this,
        &StatFs::handleChannelInitializationFailure);
    connect(m_channel.data(), &SftpChannel::finished,
        this, &StatFs::handleSftpJobFinished);
    connect(m_channel.data(),
        &SftpChannel::vfsInfoAvailable,
        this, &StatFs::handleStatVfsInfoAvailable);
    connect(m_channel.data(), &SftpChannel::closed, this,
        &StatFs::handleChannelClosed);
    m_channel->initialize();
}

void StatFs::handleDisconnected()
{
    std::cout << "stat finished. ";
    if (m_error)
        std::cout << "There were errors.";
    else
        std::cout << "No errors encountered.";
    std::cout << std::endl;
    QCoreApplication::exit(m_error ? EXIT_FAILURE : EXIT_SUCCESS);
}

void StatFs::handleError()
{
    std::cerr << "Encountered SSH error: "
        << qPrintable(m_connection->errorString()) << "." << std::endl;
    m_error = true;
}

void StatFs::handleChannelInitialized()
{
    qDebug() << "Channel initialized";
    if (m_statJob != SftpInvalidJob) {
        qWarning() << "Already had called statjob!";
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }
    m_statJob = m_channel->statVfs(m_parameters.remotePath);
    qDebug() << "Launched statfs job" << m_statJob;
}

void StatFs::handleChannelInitializationFailure(const QString &reason)
{
    std::cerr << "Could not initialize SFTP channel: " << qPrintable(reason)
        << "." << std::endl;
    earlyDisconnectFromHost();
}

void StatFs::handleChannelClosed()
{
    if (m_statJob == SftpInvalidJob) {
        qDebug() << "Channel closed before statfs called";
    } else {
        std::cout << "SFTP channel closed. Now disconnecting..." << std::endl;
    }
    m_connection->disconnectFromHost();
}

void StatFs::handleSftpJobFinished(SftpJobId job, const SftpError errorType, const QString &error)
{
    Q_UNUSED(errorType);
    if (errorType != QSsh::NoError) {
        qWarning() << job << "finished with error type" << errorType;
    }

    if (!error.isEmpty()) {
        qWarning() << job << "finished with error" << error;
    }
    if (job != m_statJob) {
        qWarning() << "Unexpected job id" << job;
    }
    qDebug() << "Job" << job << "finished";

    m_channel->closeChannel();
}

void StatFs::handleStatVfsInfoAvailable(QSsh::SftpJobId job, const QSsh::SftpVfsInfo &vfsInfo)
{
    printf("Received info:\n"
           " bsize: %llu\n"
           " frsize: %llu\n"
           " blocks: %llu\n"
           " bfree: %llu\n"
           " bavail: %llu\n"
           " files: %llu\n"
           " ffree: %llu\n"
           " favail: %llu\n"
           " fsid: %llu\n"
           " flag: %llu\n"
           " namemax: %llu\n",
           vfsInfo.blockSize,
           vfsInfo.fundamentalBlockSize,
           vfsInfo.blocksTotal,
           vfsInfo.blocksFree,
           vfsInfo.blocksAvailable,
           vfsInfo.inodesTotal,
           vfsInfo.inodesFree,
           vfsInfo.inodesAvailable,
           vfsInfo.filesystemId,
           vfsInfo.flags,
           vfsInfo.maxFilenameLength
      );
}

void StatFs::earlyDisconnectFromHost()
{
    qDebug() << "Errored out, disconnecting from host";
    m_error = true;
    if (m_channel) {
        disconnect(m_channel.data(), nullptr, this, nullptr);
    }
    m_connection->disconnectFromHost();
}
